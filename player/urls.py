
from django.urls import path
from player.views import search_player, show_player

urlpatterns = [
    path("results/", search_player, name="search_results"),
    path("<int:player_id>", show_player, name="show_player"),
]