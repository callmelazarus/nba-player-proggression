from django.urls import reverse_lazy
from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
import requests
# Home page

# lebron ID = 265


# url = "https://api-nba-v1.p.rapidapi.com/players/statistics"

# querystring = {"id":"265","season":"2020"}


# response = requests.request("GET", url, headers=headers, params=querystring)

# # print(response.text)

# data = response.json()
# data

# Player search page


def search_player(request):
    if request.method == "POST":
        searched = request.POST['test'] # this bypasses the need to create a form. The string in the brackets is is the search name in the html 
        url = "https://api-nba-v1.p.rapidapi.com/players"
        querystring = {"search":searched}
        headers = {"X-RapidAPI-Key": "f5f8f25546msh7978172694bd117p110085jsna7819083cca9",
        "X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com"}
        response = requests.request("GET", url, headers=headers, params=querystring)
        player_searched = response.json()
        players = player_searched.get("response") # players is a list
        context = {
        'searched': searched,
        'players': players,
        }
        return render(request, 'player/searchresults.html', context)
    else:
        return render(request, 'player/searchresults.html', {})


# how can we pass that id into the detail page 


def convert_to_secs(mins):
    if mins == None:
        return 0
    mins_secs = mins.split(":")
    mins = int(mins_secs[0])
    secs = int(mins_secs[1])
    secs += mins * 60
    return secs

    


def find_season_averages(player_id):
    career_stats = {}
    seasons = range(2015, 2017)
    years_played = []
    url = "https://api-nba-v1.p.rapidapi.com/players/statistics"
    headers = {
	"X-RapidAPI-Key": "f2a2d07153mshed8570c56eee9b9p18988cjsna4d647e380e1",
	"X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com"
    }

    for season in range(2015, 2017):
        querystring = {"id":str(player_id),"season":str(season)}
        response = requests.request("GET", url, headers=headers, params=querystring)
        season_data = response.json()
        if len(season_data["response"])> 0:
            years_played.append(season)
            stats = {
                "Season": season,
                "points": 0,
                "min": 0,
                "fgm": 0,
                "fga": 0,
                "fgp": 0,
                "ftm": 0,
                "fta": 0,
                "ftp": 0,
                "tpm": 0,
                "tpa": 0,
                "tpp": 0,
                "offReb": 0,
                "defReb": 0,
                "totReb": 0,
                "assists": 0,
                "pFouls": 0,
                "steals": 0,
                "turnovers": 0,
                "blocks": 0,
                "plusMinus": 0
            }
            games = 0
            for game in season_data["response"]:
                if convert_to_secs(game["min"]) > 0:
                    games += 1
                    stats["points"] += float(game["points"])
                    stats["min"] += convert_to_secs(game["min"])
                    stats["fgm"]+= float(game["fgm"])
                    stats["fga"]+= float(game["fga"])
                    stats["fgp"]+= float(game["fgp"])
                    stats["ftm"]+= float(game["ftm"])
                    stats["fgm"]+= float(game["fgm"])
                    stats["fta"]+= float(game["fta"])
                    stats["ftp"]+= float(game["ftp"])
                    stats["tpm"]+= float(game["tpm"])
                    stats["tpa"]+= float(game["tpa"])
                    stats["tpp"]+= float(game["tpp"])
                    stats["offReb"]+= float(game["offReb"])
                    stats["defReb"]+= float(game["defReb"])
                    stats["totReb"]+= float(game["totReb"])
                    stats["assists"]+= float(game["assists"])
                    stats["pFouls"]+= float(game["pFouls"])
                    stats["steals"]+= float(game["steals"])
                    stats["turnovers"]+= float(game["turnovers"])
                    stats["blocks"]+= float(game["blocks"])
                    stats["plusMinus"]+= float(game["plusMinus"])

            
            for key, value in stats.items():
                if key != "Season":
                    stats[key]  = stats[key]/ games
                    stats[key] = round(stats[key], 2)
            
            stats["min"] = stats["min"] // 60
        
            career_stats[str(season)] = stats
        else:
            career_stats=None
        
    return career_stats
    



# grabs every game for a season fora player, averages themand returns those stats as a dict

#player detail page
def show_player(request, player_id):
    # if request.method == "POST": # this may need to be placed in every view function where we want this functionality. Problem: Cannot deal wtih multiple post requests
    #     return search_player(request)

    career_stats = find_season_averages(player_id) # dictionaries
    if career_stats == None:
        return render(request, "player/emptystats.html", {})
    else:
        categories = None
        for key, value in career_stats.items():
            categories = list(value.keys())
        context = {
            "categories": categories,
            "years": career_stats,  # list of dictionaries for each season's stats
        }
        return render(request, "player/detail.html", context)



# 1. find seasons for a player
# 2. get games per season for a player for each season
# 3. create averages for stats per season

